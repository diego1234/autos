<?php

namespace App\Http\Controllers;
use App\Model\auto;
use Illuminate\Http\Request;

class autoController extends Controller
{
    public function index()
    {
        return auto::all();
    }

    public function store (Request $request)
    {
        $auto = new auto();
        $auto->fill($request->toArray())->save();
        
        return $auto;

    }
}
