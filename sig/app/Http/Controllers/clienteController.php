<?php

namespace App\Http\Controllers;
use App\Model\cliente;
use Illuminate\Http\Request;


class clienteController extends Controller
{
    public function index()
    {
        return cliente::all();
    }

    public function store (Request $request)
    {
        $cliente = new cliente();
        $cliente->fill($request->toArray())->save();
        
        return $cliente;

    }
    public function destroy($id)
    {
        $cliente = cliente::query()->where('id',$id)->first();
        return $cliente->delete();

    }
}
