<?php

namespace App\Http\Controllers;
use App\Model\costo;
use Illuminate\Http\Request;

class costoController extends Controller
{
    public function index()
    {
        return costo::all();
    }

    public function store (Request $request)
    {
        $costo = new costo();
        $costo->fill($request->toArray())->save();
        
        return $costo;

    }
}
