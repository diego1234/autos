<?php

namespace App\Http\Controllers;
use App\Model\documento;
use Illuminate\Http\Request;

class documentoController extends Controller
{
    public function index()
    {
        return documento::all();
    }

    public function store (Request $request)
    {
        $documento = new documento();
        $documento->fill($request->toArray())->save();
        
        return $documento;

    }
}
