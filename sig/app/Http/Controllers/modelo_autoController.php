<?php

namespace App\Http\Controllers;
use App\Model\modelo_auto;
use Illuminate\Http\Request;

class modelo_autoController extends Controller
{
    public function index()
    {
        return modelo_auto::all();
    }

    public function store (Request $request)
    {
        $modeloauto = new modelo_auto();
        $modeloauto->fill($request->toArray())->save();
        
        return $modeloauto;

    }
}
