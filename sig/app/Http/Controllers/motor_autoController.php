<?php

namespace App\Http\Controllers;
use App\Model\motor_auto;
use Illuminate\Http\Request;

class motor_autoController extends Controller
{
    public function index()
    {
        return motor_auto::all();
    }

    public function store (Request $request)
    {
        $motorauto = new motor_auto();
        $motorauto->fill($request->toArray())->save();
        
        return $motorauto;

    }
}
