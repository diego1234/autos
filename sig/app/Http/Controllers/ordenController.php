<?php

namespace App\Http\Controllers;
use App\Model\orden;
use Illuminate\Http\Request;

class ordenController extends Controller
{
    public function index()
    {
        return orden::all();
    }

    public function store (Request $request)
    {
        $orden = new orden();
        $orden->fill($request->toArray())->save();
        
        return $orden;

    }


}
