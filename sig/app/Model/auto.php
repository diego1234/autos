<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class auto extends Model
{
    protected $table = 'auto';

    protected $primarykey = 'id_auto';
    public $timestamps = true;
    const CREATED_AT = 'created_ad';
    const UPDATED_AT = 'updated_ad';



    protected $fillabel = [
      'tamanio',
      'transmision',
      'color',
      'condicion'
    ];
}
