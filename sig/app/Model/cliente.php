<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
    protected $table = 'cliente';

    protected $primarykey = 'id';
    public $timestamps = true;
    const CREATED_AT = 'created_ad';
    const UPDATED_AT = 'updated_ad';



    protected $fillabel = [
      'nombre',
      'apellido',
      'CI',
      'telefono',
      'domicilio_direccion'
    
    
    
    
    
    ];

}
