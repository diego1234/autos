<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class costo extends Model
{
    protected $table = 'costo';

    protected $primarykey = 'id_costo';
    public $timestamps = true;
    const CREATED_AT = 'created_ad';
    const UPDATED_AT = 'updated_ad';



    protected $fillabel = [
      'precio',
      'rebaja',
      'total'
      
    
    ]; 
}
