<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class documento extends Model
{
    protected $table = 'documento';

    protected $primarykey = 'id_documento';
    public $timestamps = true;
    const CREATED_AT = 'created_ad';
    const UPDATED_AT = 'updated_ad';



    protected $fillabel = [
      'nombre_completo',
      'licencia_conducir',
      'categoria',
      'domicilio',
      'fecha'

    
    ]; 
}
