<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class modelo_auto extends Model
{
    protected $table = 'modelo_auto';

    protected $primarykey = 'id_modelo';
    public $timestamps = true;
    const CREATED_AT = 'created_ad';
    const UPDATED_AT = 'updated_ad';



    protected $fillabel = [
      'modelo',
      'industria',
      'empresa',
      'tipo'

    
    ]; 
}
