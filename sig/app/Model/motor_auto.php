<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class motor_auto extends Model
{
    protected $table = 'motor_auto';

    protected $primarykey = 'id_motor';
    public $timestamps = true;
    const CREATED_AT = 'created_ad';
    const UPDATED_AT = 'updated_ad';



    protected $fillabel = [
      'cilindrada',
      'kilometraje',
      'tipo_motor',
      'numero_motor'

    
    ]; 
}
