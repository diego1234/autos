<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class orden extends Model
{
    protected $table = 'orden';

    protected $primarykey = 'id_orden';
    public $timestamps = true;
    const CREATED_AT = 'created_ad';
    const UPDATED_AT = 'updated_ad';



    protected $fillabel = [
      'numero_orden',
      'cantidad',
      'observaciones'

    
    ];
}
