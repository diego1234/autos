<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModeloAutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modelo_auto', function (Blueprint $table) {
            $table->bigIncrements('id_modelo');
            $table->string('modelo',100);
            $table->string('industria',100);
            $table->string('empresa',100);
            $table->string('tipo',100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modelo_auto');
    }
}
