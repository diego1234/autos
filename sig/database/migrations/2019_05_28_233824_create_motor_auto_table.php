<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotorAutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motor_auto', function (Blueprint $table) {
            $table->bigIncrements('id_motor');
            $table->string('cilindrada',100);
            $table->string('kilometraje',100);
            $table->string('tipo_motor',100);
            $table->string('numero_motor',100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motor_auto');
    }
}
