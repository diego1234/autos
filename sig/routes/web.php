<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/cliente','clienteController@show');
Route::get('/orden','ordenController@show');
Route::get('/costo','costoController@show');
Route::get('/documento','documentoController@show');
Route::get('/auto','autoController@show');
Route::get('/modelo_auto','modelo_autoController@show');
Route::get('/motor_auto','motor_autoController@show');
